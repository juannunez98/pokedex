import React from 'react';
import '../css/details.css'

function Stats(props) {
    if (props.stats !== undefined) {
        let stats = props.stats

        return (<div className="stats-container w-5 grid" >
            <h2>Stats</h2>
            {
                stats.map((item, index) => {
                    return (<div className="item-container" key={index} >

                        <h3>{item.stat.name}</h3>
                        <p>{item.base_stat}</p>

                    </div>)
                })
            }
        </div>)
    } else {
        return (<div className="stats-container" ></div>)
    }
}

export default Stats