import React from "react";
import '../css/details.css'

function TypeDetailsPills(props) {
    if (props.data !== undefined) {
        return (
            <div className="grid">
                <h2>{props.title}</h2>
                {
                    props.data.map((element, i) => {
                        return (
                            <div className="item-container" key={i} >
                                <p>
                                    {element.name}
                                </p>
                            </div>
                        )
                    })
                }
            </div>
        )

    } else {
        return <div></div>
    }
}

export default TypeDetailsPills