import React from 'react';
import '../css/details.css'

function Abilities(data) {
    if (data.data !== undefined) {
        let abilities = data.data

        return (<div className="abilities-container grid" >
            <h2>Abilities</h2>
            {
                abilities.map((item, index) => {
                    return (<div className="item-container" key={index} >{
                        <p>{item.ability.name}</p>
                    }</div>)
                })
            }
        </div>)
    } else {
        return (<div className="abilities-container" ></div>)
    }

}

export default Abilities