import React from 'react';
import '../css/details.css'

function Movements(props) {
    if (props.movements !== undefined) {
        let movements = props.movements
        return (<div className="movements-container w-100 grid">
            <h2>Movements</h2>
            {
                movements.map((item, index) => {
                    return (<div key={index} className="item-container">
                        {
                            <p>{item.move.name}</p>
                        }
                    </div>)
                })
            }
        </div>)
    } else {
        return (<div className="movements-container"></div>)
    }
}

export default Movements