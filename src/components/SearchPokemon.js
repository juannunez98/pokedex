import React, { useState, useEffect } from "react";
import "../css/menu.css"

function SearchPokemon(props) {
    const limit = 20
    const [pokemonUrl, setPokemonUrl] = useState(`https://pokeapi.co/api/v2/pokemon?offset=0&limit=${limit}`)
    const [data, setData] = useState([])

    useEffect(() => {
        const getData = async () => {
            const res = await fetch(pokemonUrl)
            const jsonResponse = await res.json()
            setData(jsonResponse)
        }

        getData()
    }, [pokemonUrl])


    const jumpPage = (page) => {
        setPokemonUrl(page)
    }

    const getPokemonId = (route) => {
        let id = route.split("pokemon/")[1]
        id = id.split("/")[0]

        return id
    }

    if (data.results !== undefined) {
        return (
            <section className="preview-pokemon-container" >

                <h3 className="title">Pokemons</h3>

                <div className="card-container">
                    {
                        data.results.map((element) => {
                            let key = getPokemonId(element.url)
                            return (
                                <button className="card" key={key} id={key} onClick={props.getDetails.bind(this, key)} >{element.name}</button>
                            )
                        })
                    }
                </div>

                <div className="pagination">
                    <div className="bar">
                        <button className="item" onClick={jumpPage.bind(this, data.previous)}> Prev </button>
                        <button className="item" onClick={jumpPage.bind(this, data.next)}> Sig </button>
                    </div>
                </div>
            </section>
        )
    } else {
        return (
            <section className="Preview-pokemon-container" >
                <h1>Pokemons</h1>
                <div className="card-container">

                </div>

                <div className="pagination">
                    <div className="bar">

                    </div>
                </div>
            </section>
        )
    }

}

export default SearchPokemon