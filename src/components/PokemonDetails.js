import React, { useState, useEffect } from 'react';
import Abilities from './Abilities'
import Movements from './Movements'
import Stats from './Stats'
import Types from './Types'
import '../css/details.css'
import TypeDetails from './TypeDetails'


function PokemonDetails(props) {
    const [data, setData] = useState([])
    const [typeId, setTypeId] = useState("")
    let menu = document.getElementById("menu");

    useEffect(() => {
        const url = `https://pokeapi.co/api/v2/pokemon/${props.pokemon}/`
        fetch(url).then(res => res.json()).then(response => {
            setData(response)

        })
    }, [props.pokemon])

    const menuTrigger = () => {
        if (menu.classList.contains('show-menu')) {

            menu.classList.remove('show-menu')
        } else {
            menu.classList.add('show-menu')
        }
    }

    const setType = (id) => {
        setTypeId(`https://pokeapi.co/api/v2/type/${id}`)
    }

    return (
        <section className="pokemon-details-container">
            <button className="filter-slider" onClick={menuTrigger}>
                Menú
            </button>
            <h1>{data.name}</h1>
            <div className="basic-information-container grid" >
                <h2>Basic information</h2>
                <div className="item-container">
                    <h3>Name</h3>
                    <p>{data.name}</p>
                </div>
                <div className="item-container">
                    <h3>Height</h3>
                    <p>{data.height}</p>
                </div>
                <div className="item-container">
                    <h3>Weight</h3>
                    <p>{data.weight}</p>
                </div>
                <div className="item-container">
                    <h3>Base experience</h3>
                    <p>{data.base_experience}</p>
                </div>
            </div>
            <Stats stats={data.stats} ></Stats>
            <Types types={data.types} getType={setType} ></Types>
            <TypeDetails url={typeId} ></TypeDetails>
            <Abilities data={data.abilities} ></Abilities>
            <Movements movements={data.moves} ></Movements>

        </section>
    )




}

export default PokemonDetails