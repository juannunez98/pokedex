import React, { useState, useEffect } from "react";
import '../css/details.css'
import Pill from './TypeDetailsPills'

function TypeDetails(props) {
    const [damage, setDamage] = useState([])


    useEffect(() => {
        fetch(props.url).then(res => res.json()).then(
            response => {
                setDamage(response.damage_relations)
            }
        )
    }, [props.url])



    return (

        <React.Fragment>
            <Pill data={damage.double_damage_from} title="Double damage from"></Pill>
            <Pill data={damage.double_damage_to} title="Double damage to"></Pill>
            <Pill data={damage.half_damage_from} title="Half damage from"></Pill>
            <Pill data={damage.half_damage_from} title="Half damage to"></Pill>
            <Pill data={damage.no_damage_from} title="No damage from"></Pill>
            <Pill data={damage.no_damage_to} title="No damage to"></Pill>
        </React.Fragment>

    )


}

export default TypeDetails