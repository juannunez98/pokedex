import React, { useState } from "react";
import "../css/App.css";
import Menu from "./Menu"
import Details from "./PokemonDetails"


function App() {
  const [currentPokemon, setCurrentPokemon] = useState(1)

  const getPokemonDetails = (id) => {
    setCurrentPokemon(id)
  }

  return (
    <div className="App" >
      <Menu getDetails={getPokemonDetails} ></Menu>
      <Details pokemon={currentPokemon} ></Details>
    </div>
  )


}


export default App;
