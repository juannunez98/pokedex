import React from 'react';
import '../css/details.css'

function Types(props) {

    const getTypeIdByUrl = (url) => {
        let id = url.split('type/')[1]
        id = id.replace('/', "")
        return id
    }

    if (props.types !== undefined) {
        let types = props.types
        return (<div className="types-container grid" >
            <h2>Types</h2>
            {
                types.map((item, index) => {
                    let id = getTypeIdByUrl(item.type.url)
                    return (<div className="item-container" key={id} >
                        <button onClick={() => props.getType(id)} >{item.type.name}</button>
                    </div>)
                })
            }
        </div>)
    } else {
        return (<div className="types-container center" ></div>)
    }
}

export default Types