import React from "react";
import "../css/menu.css"
import SearchPokemon from './SearchPokemon'
function Menu(props) {
  return (
    <section id="menu" className="menu-container">
      <SearchPokemon getDetails={props.getDetails} ></SearchPokemon>
    </section>
  )
}


export default Menu;
